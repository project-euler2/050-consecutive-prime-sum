import math
import time
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(a,n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(a,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def consecutive_prime_sum(max_limit):
    primes = Prime_Number_In_Range(2,max_limit)
    highest_consecutive_prime = 0
    highest_consecutive = 0
    for j in range(len(primes) - 1):
        num = 0
        i = 0
        while num <= max_limit:
            num += primes[i+j]
            if num < max_limit and is_prime(num) and i > highest_consecutive: 
                highest_consecutive_prime = num
                highest_consecutive = i
            i+=1  
    return highest_consecutive_prime

print(consecutive_prime_sum(1000000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )